<?php

namespace TimChebotarev\Messenger;

interface MessengerContract
{
    /**
     * Send message
     *
     * @param  string  $directory
     * @return bool
     */
    public function send($recipient, $message);
}
