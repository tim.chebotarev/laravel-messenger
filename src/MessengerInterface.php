<?php

namespace TimChebotarev\Messenger;

interface MessengerInterface
{
    /**
     * Send message
     *
     * @param  string  $directory
     * @return bool
     */
    public function send($recipient, $message);
}
