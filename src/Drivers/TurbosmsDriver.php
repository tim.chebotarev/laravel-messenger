<?php
/**
 * Created by PhpStorm.
 * User: timotei
 * Date: 17.06.2017
 * Time: 0:47
 */

namespace TimChebotarev\Messenger\Drivers;

use TimChebotarev\Messenger\MessengerInterface;

class TurbosmsDriver implements MessengerInterface
{
	protected $user;
	protected $password;
	protected $sender;
	protected $client;

	/**
	 * TurbosmsDriver constructor.
	 */
	public function __construct($config)
	{
		$this->user = $config['user'];
		$this->password = $config['password'];
		$this->sender = $config['sender'];

		$this->client = new \SoapClient('http://turbosms.in.ua/api/wsdl.html');

		$auth = [
			'login' => $this->user,
			'password' => $this->password
		];
		$this->client->Auth($auth);
	}

	/**
	 * Send message
	 *
	 * @param $recipient
	 * @param $message
	 * @return boolean
	 */
	public function send($recipient, $message)
	{
//		$result->bbalance = $client->GetCreditBalance();
		try {
			$sms = [
				'sender' => $this->sender,
				'destination' => $recipient,
				'text' => $message
			];

			$result = $this->client->SendSMS($sms);

//			$msgId = $result->SendSMSResult->ResultArray[1];
//
//			$sms = ['MessageId' => $msgId];
//
//			sleep(5);
//			$result->dstat = $client->GetMessageStatus($sms);
			return true;
		}
		catch(\Exception $e) {
			return false;
		}
	}
}