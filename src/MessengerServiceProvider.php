<?php

namespace TimChebotarev\Messenger;

use Illuminate\Support\ServiceProvider;

class MessengerServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = true;

	/**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		$this->app->singleton('messages', function () {
			return new Messenger;
		});

		$this->registerManager();
    }

    /**
     * Register the messenger manager.
     *
     * @return void
     */
    protected function registerManager()
    {
        $this->app->singleton('messenger', function () {
            return new MessengerManager($this->app);
        });
    }

    /**
     * Get the default messages driver.
     *
     * @return string
     */
    protected function getDefaultDriver()
    {
        return $this->app['config']['messenger.default'];
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [
			'messenger',
		];
	}
}
