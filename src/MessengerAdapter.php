<?php

namespace TimChebotarev\Messenger;

use RuntimeException;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Support\Collection;
use TimChebotarev\Messenger\MessengerContract;

class MessengerAdapter implements MessengerContract
{
    /**
     * The Messenger implementation.
     *
     * @var MessengerInterface
     */
    protected $driver;

    /**
     * Create a new filesystem adapter instance.
     *
     * @param  MessengerInterface  $driver
     * @return void
     */
    public function __construct(MessengerInterface $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Send message.
     *
     * @param  string  $recipient
     * @param  string  $message
     * @return boolean
     */
    public function send($recipient, $message)
    {
        return $this->driver->send($recipient, $message);
    }

    /**
     * Pass dynamic methods call onto Flysystem.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, array $parameters)
    {
        return call_user_func_array([$this->driver, $method], $parameters);
    }
}
