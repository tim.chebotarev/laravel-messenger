<?php

namespace TimChebotarev\Messenger;

interface Factory
{
    /**
     * Get a messenger implementation.
     *
     * @param  string  $name
     * @return MessengerContract
     */
    public function messenger($name = null);
}
